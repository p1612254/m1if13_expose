export class GetUserDto {
    constructor(
        public login: string, 
        public connected: boolean
    ) {}
}