export class User {
    private _connected: boolean = false;
    constructor(
        private readonly _login: string,
        private _password: string
    ) {}

    get login() {
        return this._login;
    }

    set password(password: string) {
        this._password = password; 
    }

    get connected() {
        return this._connected
    }

    public authenticate(password: string): void {
        if (password !== this.password) {
            throw new Error("Erroneous password");
        }
        this._connected = true;
    }  

    public disconnected(): void {
        this._connected = false;
    }
}
