import { Controller, Get, Post, Body, Param, Delete, Put, HttpCode, Res, Header } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Response } from 'express';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto, @Res() response: Response) {
    this.usersService.create(createUserDto);
    response.setHeader("Location", "/users/"+createUserDto.login)
    .send();
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }
  
  @Get(':login')
  findOne(@Param('login') login: string) {
    return this.usersService.findOne(login);
  }

  @Put(':login')
  @HttpCode(204)
  update(@Param('login') login: string, @Body() updateUserDto: UpdateUserDto) {
    this.usersService.update(login, updateUserDto);
  }

  @Delete(':login')
  @HttpCode(204)
  remove(@Param('login') login: string) {
    this.usersService.remove(login);
  }
}
