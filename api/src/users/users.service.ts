import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { GetUserDto } from './dto/get-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';[]

@Injectable()
export class UsersService {
  private readonly users: User[] = [];

  create(createUserDto: CreateUserDto) : void {
    this.findUser(createUserDto.login, true);
    const user = new User(createUserDto.login, createUserDto.password);
    this.users.push(user);
  }

  findAll(): string[] {
    return this.users.map((user) => {
      return "users/"+user.login;
    });
  }

  findOne(login: string) : GetUserDto {
    const user = this.findUser(login);
    return new GetUserDto(user.login, user.connected);
  }

  update(login: string, updateUserDto: UpdateUserDto): void {
    const user = this.findUser(login);
    user.password = updateUserDto.password;
  }

  remove(login: string): void {
    const pos = this.users.findIndex((user) => user.login===login);
    this.users.splice(pos, 1);
  }

  findUser(login: string, checkConflict: boolean=false): User {
    const user = this.users.find((user) => user.login===login);
    if(user == null && !checkConflict) {
      throw new NotFoundException("User does not exist");
    } else if (user != null && checkConflict) {
      throw new ConflictException("A user already exists with this login");
    }
    return user;
  }
}
