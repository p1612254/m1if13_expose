import { Chalk } from "chalk";
import { Command, CommandRunner} from "nest-commander";
import { Questions } from "./game.questions";

const chalk: Chalk = require('chalk');
const chalk_animation = require("chalk-animation");
const figlet = require("figlet");
const gradient = require("gradient-string");

const sleep = (time=2000) => new Promise((res) => setTimeout(res, time));

@Command({name: "game", options: {isDefault: true}})
export class GameCommand implements CommandRunner {
    constructor(private questions: Questions) {}
    async run(passedParams: string[], options?: Record<string, any>): Promise<void> {
        await this.welcome();
        await this.questions.quizz();
        await this.win();
    }

    async welcome() {
        const rainbowTitle = chalk_animation.rainbow(
            "✨ Qui veut être un NestMillionnaire ? ✨"
        );
        await sleep(4000);
        rainbowTitle.stop();

        console.log(`
            ${chalk.bgBlue("LES RÈGLES ⚠️")}
            Je suis un processus sur ton ordinateur.
            Si tu réponds faux à UNE question, je serai ${chalk.bgRed("tué")} 💀!
            Donc fait bien attention 🙏 ! 
        `);
    }

    async win() {
        let winMsg = `Félécitations ! vous remportez 
        1 000 000 $`;
        figlet(winMsg, (err, data) => {
            console.clear();
            console.log(gradient.pastel.multiline(data));
        });
    }
}