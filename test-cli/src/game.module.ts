import { Module } from '@nestjs/common';
import { GameCommand } from './game.command';
import { Questions } from './game.questions';

@Module({
  providers: [
    GameCommand,
    Questions
  ]
})
export class GameModule {}
