import { createSpinner } from "nanospinner"

const inquirer = require("inquirer");
const sleep = (time=3000) => new Promise((res) => setTimeout(res, time));


export class Questions {
    async ready() {
        const answer = await inquirer.prompt({
            name: "ready",
            type: "confirm",
            message: "Est-ce que vous êtes prêt ?"
        });
        if (!answer.ready) {
            console.log("Adieu ! 🥲");
            process.exit(1);
        }
    }
    
    async question1() {
        const answer = await inquirer.prompt({
            name: "q1",
            type: "list", 
            message: "Quelle est la commande nest-cli pour créer un nouveau projet ?",
            choices: [
                "nest-cli new <project-name>",
                "npm new cli <project-name>",
                "nest new <project-name>"
            ]
        });
        await this.handleAnswer(answer.q1==="nest new <project-name>");
    }

    async question2() {
        const answer = await inquirer.prompt({
            name: "q2",
            type: "list",
            message: "NestJs est un framework avec une architecture :",
            choices: [
                "arborescente",
                "modulaire",
                "archaïque"
            ]
        });
        await this.handleAnswer(answer.q2==="modulaire");
    }

    async question3() {
        const answer = await inquirer.prompt({
            name: "q3",
            type: "list",
            message: "Avec NestJs, les middlewares sont :",
            choices: [
                "définis avec une annotation",
                "définis comme en TypeScript",
                "définis avec une classe implémentant NestMiddleware"
            ]
        });
        await this.handleAnswer(answer.q3==="définis avec une classe implémentant NestMiddleware");
    }

    async question4() {
        const answer = await inquirer.prompt({
            name: "q4",
            type: "list",
            message: "Le gestionnaire d'exceptions est défini :",
            choices: [
                "par défaut par NestJs",
                "avec un fichier de configuration XML",
                "avec un contrôleur spécifique"
            ]
        });
        await this.handleAnswer(answer.q4==="par défaut par NestJs");
    }

    async question5() {
        const answer = await inquirer.prompt({
            name: "q5",
            type: "list",
            message: "A votre avis, que représente le logo de NestJS ?",
            choices: [
                "un tigre",
                "un chat",
                "un lion"
            ]
        });
        await this.handleAnswer(answer.q5==="un chat");
    }

    async handleAnswer(isCorrect) {
        const spinner = createSpinner("Vérification...").start();
        await sleep();
        if (!isCorrect) {
            spinner.error({text: "💀💀💀 Perdu. Adieu...", mark: '❌'});
            process.exit(1);
        }
        spinner.success({text: "Bien joué !", mark: "✔️"});
    }

    async quizz() {
        await this.ready();
        await this.question1();
        await this.question2();
        await this.question3();
        await this.question4();
        await this.question5();
    }
}