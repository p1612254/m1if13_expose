#!/usr/bin/env node
import { CommandFactory } from 'nest-commander';
import { GameModule } from './game.module';

async function bootstrap() {
  await CommandFactory.run(GameModule);

}
bootstrap();
